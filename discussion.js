//CRUD Operations

//Insert Documents (Create)
/*
	Syntax:
		Insert One Document
			db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})

		Insert Many Documents
			db.collectionName.insertMany([
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				},
				{
					"fieldA": "valueA",
					"fieldB": "valueB"
				}
			])
*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
});

db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Curry",
		"age": 34,
		"email": "babyfacedassassin30@mail.com",
		"department": "Golden State Warriors"
	},
	{
		"firstName": "Giannis",
		"lastName": "Antetokuompo",
		"age": 27,
		"email": "greekfreak@mail.com",
		"department": "Milwaukee Bucks"
	}
]);

db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": false
	}
]);

//Find Documents (Read)
/*
	Syntax:
		db.collectionName.find() - this will retrieve all our documents
		db.collectionName.find({"criteria": "value"}) - this will retrieve all our documents that will match our criteria
		db.collectionName.findOne({}) - this will return the first document in our collection that will match out criteria
*/

db.users.find({
	"name": "Jane"
});

db.users.find({
	"department": "none"
});

db.users.findOne({
	"department": "none"
});

//Update Documents (Update)
/*
	Syntax:
		db.collectionName.updateOne({
			"criteria": "value"
		},
		{
			$set: {
				"fieldToBeUpdate": "updatedValue"
			}
		})
*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "Test@mail.com",
	"department": "none"
});

//Updating One Document
db.users.updateOne(
	{
		"firstName": "Test"
	},
	{
		$set: {
			"firstName": "Shai",
			"lastName": "Gildeus-Alexander",
			"age": 24,
			"email": "sga5@mail.com",
			"department": "Oklahoma City Thunder",
			"status": "active"
		}
	}
);

//Updating Multiple Documents
db.users.updateMany(
	{
		"department": "none"
	},
	{
		$set{
			"department": "HR"
		}
	}
);

db.users.updateOne(
	{
		"firstName": "Jane",
		"lastName": "Doe"
	},
	{
		$set: {
			"department": "Operations"
		}
	}
);

//Removing a field
db.users.updateOne(
	{
		"firstName": "Giannis"
	},
	{
		$unset: {
			"status": "active"
		}
	}
);

db.users.updateMany(
	{},
	{
		$rename: {
			"department": "dept"
		}
	}
);

db.courses.updateOne(
	{
		"name": "HTML 101"
	},
	{
		$set: {
			"isActive": false
		}
	}
);

db.courses.updateMany(
	{},
	{
		$set: {
			"enrollees": 10
		}
	}
);

//Deleting Documents (Remove)
/*
	Syntax:
		db.collectionName.deleteOne({"criteria": "value"})
*/

db.users.insertOne({
	"firstName": "Test"
});

db.users.deleteOne({"firstName": "Test"});

db.users.deleteMany({"dept": "HR"});

db.courses.deleteMany({});

